#Makefile para levantar testear y bajar las imagenes docker, ejecutar en el mismo directorio donde esta el repo
all: docker_up testing_nginx docker_down
docker_build:
	docker build --no-cache -t reto-devops_node_service -f Dockerfile .
	docker build --no-cache -t reto-devops_nginx_service -f ./nginx/Dockerfile ./nginx
docker_up:
	docker-compose up --build -d
testing_nginx:
	timeout 5 echo "Testeando la api de la app a traves de nginx"
	curl http://localhost:8081/
	curl http://localhost:8081/public
	curl http://localhost:8081/private
	curl -u superuser1:password http://localhost:8081/private
docker_down:
	docker-compose down
