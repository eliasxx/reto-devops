#Se utiliza de base la imagen alpine para disminuir el tamaño de la imagen final
#para ello se instala solo lo necesario en este caso nodejs y npm
#luego se realiza la instalacion de las librerias de produccion dado que en la imagen final no necesitamos las librerias de desarrollo
#luego se genera una nueva imagen donde solo se instala nodejs y se copia del build anterior los node_modules se genera un usuario distinto de root
#y se copia el codigo fuente exponiendo el puerto 3000 y ejecutando con node el archivo index.js
FROM alpine AS builder
WORKDIR /usr/app
RUN apk add --no-cache --update nodejs nodejs-npm
COPY package.json package-lock.json ./
RUN npm install --production --quiet
FROM alpine
ARG USER=node_user
WORKDIR /usr/app
RUN apk add --no-cache --update nodejs
ENV NODE_ENV production
RUN adduser -D $USER
USER $USER
COPY --from=builder /usr/app/node\_modules ./node\_modules
COPY . .
EXPOSE 3000
CMD ["node", "index.js"]